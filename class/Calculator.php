<!DOCTYPE html>
<html lang="en">

<?php include_once "../builders/head.html";
class Calculate
{
    public function calc($num1, $num2){
        echo ($num1 . " + " . $num2 . " = " . ($num1 + $num2) . "<br>");
        echo ($num1 . " * " . $num2 . " = " . ($num1 * $num2) . "<br>");
        if ($num1 and $num2 != 0){
            echo ($num1 . " / " . $num2 . " = " . ($num1 / $num2) . "<br>");
            echo ($num1 . " % " . $num2 . " = " . ($num1 % $num2) . "<br>");
        }
        else{
            echo "can't divide by zero";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row justify-content-center align-self-center">
        
            <?php
                        $calc = new Calculate();
                        $calc->calc($_GET['num1'], $_GET['num2']);
            ?>
    </div>
    <div class="row justify-content-center align-self-center">
        <form action="/index.php">
            <input type="submit" value="Go back" />
        </form>
    </div>
</div>  
</body>
</html>     