<!DOCTYPE html>
<html lang="en">
<?php include_once "builders/head.html" ?>
<body>
<div class="container-fluid">
    <div class="row justify-content-center align-self-center">
        <h1>
            Calculator
        </h1>
    </div>
    <div class="row justify-content-center align-self-center">
            <!-- FORM for calculation GET/POST -->
        <form method="get" action="class/Calculator.php">
            <div class="form-group">
                Num 1 <input type="number" name="num1" required>
            </div>
            <div class="form-group">
                Num 2 <input type="number" name="num2" required>
            </div>
            <div class="form-group">
                <input class="btn-secondary" type="submit">
            </div>
        </form>
    </div>
</div>
</body>
</html>